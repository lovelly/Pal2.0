declare namespace Db {
    enum Attr {
        base = "\u57FA\u7840\u5C5E\u6027",
        physical = "\u6B66\u672F",
        defense = "\u9632\u5FA1",
        hp = "\u4F53\u529B",
        mp = "\u771F\u6C14",
        magic = "\u7075\u529B",
        bodylaw = "\u8EAB\u6CD5",
        luck = "\u5409\u8FD0",
        roundHp = "\u6218\u6597\u4E2D\u6BCF\u56DE\u5408\u4F53\u529B",
        roundMp = "\u6218\u6597\u4E2D\u6BCF\u56DE\u5408\u771F\u6C14",
        useMagicSubMp = "\u65BD\u6CD5\u6240\u9700\u771F\u6C14\u51CF\u5C11",
        notDeBuff = "\u4E0D\u53D7\u5F02\u5E38\u72B6\u6001\u5F71\u54CD",
        fire = "\u706B\u5C5E\u6027",
        wood = "\u6728\u5C5E\u6027",
        water = "\u6C34\u5C5E\u6027",
    }
    /** 物品  1-999 装备
     * arms 武器    1-199是武器  1-30刀 31-60剑 61-80鞭 81-100刺 101-120杖 121-140伞
     * hat 帽子     200-399帽子
     * cloak 披风护肩 400-599披风护肩
     * clothes 衣服  500-699衣服
     * wear 佩戴     600-799佩戴
     * shoes 鞋子    800-999鞋子
     *
     * plot 剧情物品 1000-1200
     * addState 攻击状态类 1201-1400
     * pelt 投掷物品  1401-1600
     * aide 战斗辅助  1601-1800
     * attrup 能力提升类 1801-2000
     * drug 药品  2001-2200
     * other 其他  2201-2400
    */
    interface Item {
        id?: number;
        name: string;
        texture?: string;
        type: string;
        desc: string;
        /** 武器附加属性 */
        attr?: {
            [key: string]: any;
        };
        /** 装备武器的要求 */
        stint?: {
            [key: string]: any;
        };
    }
    function getItem(id: number): Item | undefined;
    /**
     * 剧情
     */
    interface Plot {
        /** 大类 dialog:对话层  move:移动  dat:数据 */
        class: string;
        /** 小类 */
        type: string;
        /** 剧情内容 */
        expand: PlotDialog | PlotMove | PlotDirector | PlotDatTask | PlotDatNpc | PlotDatDoor | PlotDatPack;
        /** 停止上传完成事件 默认是false */
        stopUpEvent?: boolean;
        /** 后续关联情节 */
        after?: Plot | Plot[];
    }
    /**
     * 对话
     * type dialog人物说话 sceneTip场景提示  aside旁白
     * content \n 换行    f换页
     */
    interface PlotDialog {
        /** 显示的说话人名 */
        name: string;
        /** 说话的标题 约等于name title存在优先 */
        title?: string;
        /** 显示的说话内容 */
        content: string;
        /** 说话时的表情 */
        phiz?: number;
        /** 表情显示方向 1左 2右 */
        phizDir?: number;
    }
    /**
     * 移动
     * type player npc
     */
    interface PlotMove {
        /** 移动的对象名称 */
        name: string;
        /** 移动的路径名称 */
        path: string;
    }
    /** 导演类
     * type scene:转场
    */
    interface PlotDirector {
        /** 转场名称 */
        name: string;
        /** 门 */
        door?: string;
    }
    /** 数据类
     * type addTask 接受一个任务
     */
    interface PlotDatTask {
        /** 任务ID */
        taskId: number;
    }
    /**
     * 数据类
     * type npc 更新npc 如果存在更新 否则新增
     */
    interface PlotDatNpc {
        /** Npc名称 */
        name: string;
        /** 操作 add|update|delete */
        cmd: string;
        /** 地图 如果不存在即指当前地图 */
        map?: string;
        /** 剧本 */
        plot?: Plot[];
        /** 满足某些条件,修改剧本 */
        needIf?: QM.NeedIf;
    }
    /**
     * 数据类
     * type door 更新门 如果存在更新 否则新增
     */
    interface PlotDatDoor {
        /** 门的名称 */
        name: string;
        /** 操作 */
        cmd: string;
        /** 门所在的地图,如果字段不存在，就是当前地图 */
        map?: string;
        /** 门的数据 */
        door: QM.Door;
    }
    /**
     * 数据类
     * type pack 更新背包数据
     */
    interface PlotDatPack {
        /** add添加 remove移除 */
        cmd: string;
        /** 物品ID */
        itemId: number;
        /** 物品放置到地图中使用 */
        point?: string;
        /** 物品放置时，设置是否可以拾取 */
        isPickUp?: boolean;
        /** 数量 如果没有设置默认为1 */
        number?: number;
    }
    interface Task {
        /** ID */
        id: number;
        /** 类型 1主线 2支线 */
        type: number;
        /** 场景 支线场景 */
        scene?: string;
        /** 任务描述 */
        desc: string;
        /** 奖励 */
        reward?: string;
        /** 状态 */
        state?: number;
    }
    function getTask(id: number): Task | undefined;
    /**
     * 获取 怪物 敌军信息
     * @param id
     */
    function getEnemy(id: number): {
        id: number;
        name: string;
        type: number;
        sex: number;
        texture: string;
        gold: number;
        attr: {
            hp: number;
            physical: number;
            magic: number;
            defense: number;
            bodylaw: number;
        };
        rangeReward: number[];
    } | undefined;
}
