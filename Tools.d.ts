interface Date{
    Format(format:string):string
}

interface Array<T>{
    /** 随机打乱数组 */
    shuffle():Array<T>
}

function parseTimer(count:number):string