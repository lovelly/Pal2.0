const {ccclass, property} = cc._decorator;

@ccclass
export default class BasePanel extends cc.Component {
    _close:cc.Node;
    protected isFirst:boolean = true;
    get close(){
        if(!this._close){
            this._close = this.node.getChildByName("close");
            this._close.on(cc.Node.EventType.TOUCH_END,this.hide,this);
        }
        return this._close;
    }

  

    show(){
        this.node.active = true;
        this.firstCall();
    }

    hide(){        
        cc.director.resume();
        this.node.active = false;
    }

    firstCall(){}
  

}
