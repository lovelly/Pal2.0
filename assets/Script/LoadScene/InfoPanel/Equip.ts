const {ccclass, property} = cc._decorator;

@ccclass
export default class Equip extends cc.Component {  
    /** 装备的ID */
    private _itemId:number;        
    set itemId(value){
        if(this._itemId==value) return;
        this._itemId = value
        this.show();
    }
    get itemId(){
        return this._itemId;
    }

    /** 装备的源数据 */
    private _item:Db.Item;
    get item(){
        if(!this._item){            
            this._item = Db.getItem(this.itemId)
        }
        return this._item;
    }


    private _icon:cc.Sprite;
    get icon(){
        if(!this._icon){
            this._icon = this.node.getChildByName("icon").getComponent(cc.Sprite);
        }
        return this._icon;
    }

    

    /** 显示当前装备的信息 */
    show(){                
        if(!this.item) return;        
        var name = this.item.texture||this.item.name;        
        var atlas:cc.SpriteAtlas = cc.loader.getRes("Item/Items",cc.SpriteAtlas);        
        this.icon.spriteFrame = atlas.getSpriteFrame(name);
    }

}
