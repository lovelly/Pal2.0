import BasePanel from "../BasePanel";
import Equip from "./Equip";

const {ccclass, property} = cc._decorator;

@ccclass
export default class InfoPanel extends BasePanel {
    /** 名称 */
    private nameLabel:cc.Label;
    private avatar:cc.Sprite;
    /** 等级 */
    private attrLevel:cc.Label;
    /** 经验 */
    private attrExp:cc.Label;
    /** 体力 */
    private attrHp:cc.Label;
    /** 真气 */
    private attrMp:cc.Label;
    /** 物理攻击力 */
    private attrPhysical:cc.Label;
    /** 魔法攻击力 */
    private attrMagic:cc.Label;
    /** 防御 */
    private attrDefense:cc.Label;
    /** 身法 */
    private attrBodylaw:cc.Label;
    /** 吉运 */
    private attrLuck:cc.Label;
    
    /** 帽子 */
    private equipHat:Equip;
    /** 衣服 */
    private equipClothes:Equip;
    /** 佩戴 */
    private equipWear:Equip;
    /** 披风护肩 */
    private equipCloak:Equip;
    /** 武器 */
    private equipArms:Equip;
    /** 鞋子 */
    private equipShoes:Equip;

    onLoad(){
        this.close;    
        this.nameLabel = this.node.getChildByName("avatar").getChildByName("name").getChildByName("label").getComponent(cc.Label);
        this.avatar = this.node.getChildByName("avatar").getComponent(cc.Sprite);
        var attr = this.node.getChildByName("attr");
        this.attrLevel = attr.getChildByName("level").getChildByName("value").getComponent(cc.Label);
        this.attrExp = attr.getChildByName("exp").getChildByName("value").getComponent(cc.Label);
        this.attrHp = attr.getChildByName("hp").getChildByName("value").getComponent(cc.Label);
        this.attrMp = attr.getChildByName("mp").getChildByName("value").getComponent(cc.Label);
        this.attrPhysical = attr.getChildByName("physical").getChildByName("value").getComponent(cc.Label);
        this.attrMagic = attr.getChildByName("magic").getChildByName("value").getComponent(cc.Label);
        this.attrDefense = attr.getChildByName("defense").getChildByName("value").getComponent(cc.Label);
        this.attrBodylaw = attr.getChildByName("bodylaw").getChildByName("value").getComponent(cc.Label);
        this.attrLuck = attr.getChildByName("luck").getChildByName("value").getComponent(cc.Label);       

        this.equipArms = this.node.getChildByName("ColumnR").getChildByName("arms").getComponent(Equip);
        this.equipCloak = this.node.getChildByName("ColumnR").getChildByName("cloak").getComponent(Equip);
        this.equipShoes = this.node.getChildByName("ColumnR").getChildByName("shoes").getComponent(Equip);        
        this.equipClothes = this.node.getChildByName("ColumnL").getChildByName("clothes").getComponent(Equip);
        this.equipHat = this.node.getChildByName("ColumnL").getChildByName("hat").getComponent(Equip);        
        this.equipWear = this.node.getChildByName("ColumnL").getChildByName("wear").getComponent(Equip);


        //this.node.active = false;
    }

    start(){
        this.node.active = false;
    }

    firstCall(){
        this.updateUI();
    }

    /**
     * 更新UI显示
     */
    updateUI(){
        var players = QM.game.getTeam();
        var player = players[0];

        this.nameLabel.string = player.name;
        this.avatar.spriteFrame = cc.loader.getRes("Avatar/"+player.name,cc.SpriteFrame);
        this.attrLevel.string = player.attr.level.toString();
        this.attrExp.string = player.attr.exp+"/"+ player.attr.level*50;
        this.attrHp.string = player.attr.hp.current+"/"+player.attr.hp.max;
        this.attrMp.string = player.attr.mp.current+"/"+player.attr.mp.max;
        this.attrPhysical.string = player.attr.physical.toString();
        this.attrMagic.string = player.attr.magic.toString();
        this.attrDefense.string = player.attr.defense.toString();
        this.attrBodylaw.string = player.attr.bodylaw.toString();
        this.attrLuck.string = player.attr.luck.toString();

        //获取装备信息
        var equipment = player.equipment

        this.equipArms.itemId = equipment.arms
        this.equipCloak.itemId = equipment.cloak
        this.equipClothes.itemId = equipment.clothes
        this.equipHat.itemId = equipment.hat
        this.equipShoes.itemId = equipment.shoes
        this.equipWear.itemId = equipment.wear
    }
}
