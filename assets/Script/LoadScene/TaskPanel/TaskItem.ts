const {ccclass, property} = cc._decorator;
@ccclass
export default class TaskItem extends cc.Component {
    private _text:cc.Label;
    get text(){
        if(!this._text){
            this._text = this.node.getChildByName("text").getComponent(cc.Label);
        }
        return this._text;
    }
    private _toggle:cc.Toggle;
    get toggle(){
        if(!this._toggle){
            this._toggle = this.node.getChildByName("state").getComponent(cc.Toggle);        
        }
        return this._toggle;
    }

    private _taskId:number;
    private _state:number;//1接受任务进行中 2完成任务
    private _task:Db.Task;

    set taskId(value){
        this._taskId = value;
        this.task = Db.getTask(value);
        this.node.name = value.toString();
        this.show();
    }
    get taskId(){
        return this._taskId;
    }
    set state(value){
        this._state = value;
        this.show();
    }
    get state(){
        return this._state;
    }
    set task(value){
        this._task = value;
    }
    get task(){
        return this._task;
    }


    show(){        
        if(!this.task) return;
        this.text.string = this.task.desc;                
        if(this.state==2){
            this.toggle.node.active = true;
        }else{
            this.toggle.node.active = false;
        }
    }
}
