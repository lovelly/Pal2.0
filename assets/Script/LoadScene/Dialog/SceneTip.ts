import BaseDialog from "./BaseDialog";

const {ccclass, property} = cc._decorator;

@ccclass
export default class SceneTip extends BaseDialog {
    private nameLabel:cc.Label;
    private content:cc.Label;

    onLoad(){
        this.nameLabel = this.node.getChildByName("name").getComponent(cc.Label);
        this.content = this.node.getChildByName("content").getComponent(cc.Label);        
    }

    show(){
        this.node.active = true;
        var dialog:Db.PlotDialog = this.plot.expand as Db.PlotDialog;        
        this.nameLabel.string = dialog.title||dialog.name;
        this.content.string = dialog.content;

        this.node.opacity=0;
        var a1 = cc.fadeIn(1);
        var a2 = cc.fadeOut(1);
        var an = cc.callFunc(()=>{            
            this.end();
        })        
        var seq = cc.sequence([a1,a2,an]);
        this.node.runAction(seq);
    }

}
