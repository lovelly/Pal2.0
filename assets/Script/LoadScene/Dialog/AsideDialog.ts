import BaseDialog from "./BaseDialog";

const {ccclass, property} = cc._decorator;

@ccclass
export default class AsideDialog extends BaseDialog {
    content:cc.Label;
    onLoad(){
        this.content = this.getComponent(cc.Label);
    }

    show(){
        var expand:Db.PlotDialog = this.plot.expand as Db.PlotDialog;        
        this.content.string = "";
        var index=1;
        this.schedule(()=>{
            this.content.string = expand.content.slice(0,index);            
            if(index>expand.content.length){
                this.hide();
            }
            index++;
        },0.1);
    }

    hide(){
        this.unscheduleAllCallbacks();
        this.scheduleOnce(()=>{
            this.end();
        },0.5);        
    }

}
