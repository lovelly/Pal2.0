
const {ccclass, property} = cc._decorator;

@ccclass
export default class BaseDialog extends cc.Component {
    /** 剧情源数据 */
    private _plot:Db.Plot;
    set plot(value){
        this._plot = value;               
        this.node.active = true;
        this.show();
    }
    get plot(){
        return this._plot;
    }

    /** 子类都要实现show方法 */
    show(){}

    /** 显示完成 传递事件给上层 */
    end(){        
        var event = new cc.Event.EventCustom("DialogEnd",true);
        this.node.active = false;
        this.node.dispatchEvent(event);        
    }
}
