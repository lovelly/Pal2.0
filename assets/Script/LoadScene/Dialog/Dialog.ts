import BaseDialog from "./BaseDialog";

const {ccclass, property} = cc._decorator;

@ccclass
export default class Dialog extends BaseDialog {
    
    private bgNode:cc.Node;
    private boxNode:cc.Node;
    private nameLabel:cc.Label;
    private content:cc.Label;
    private phiz:cc.Sprite;
    private phizWidget:cc.Widget;
    private boxWidget:cc.Widget;

    private fullContent:string[] = [];

    onLoad(){
        this.bgNode = this.node.getChildByName("Bg");
        this.boxNode = this.bgNode.getChildByName("Box");
        this.boxWidget = this.boxNode.getComponent(cc.Widget);
        this.nameLabel = this.boxNode.getChildByName("name").getComponent(cc.Label);
        this.content = this.boxNode.getChildByName("content").getComponent(cc.Label);
        this.phiz = this.node.getChildByName("phiz").getComponent(cc.Sprite);
        this.phizWidget = this.phiz.getComponent(cc.Widget);
        this.node.on(cc.Node.EventType.TOUCH_END,this.showItem,this);
    }

    show(){        
        var dialog:Db.PlotDialog = this.plot.expand as Db.PlotDialog;               
        this.nameLabel.string = (dialog.title||dialog.name) + ":";
        
        this.fullContent = dialog.content.split("f");
        
        this.showItem();
    }

    showItem(){
        if(!this.fullContent.length){
            this.hide();
            return;
        }

        var dialog:Db.PlotDialog = this.plot.expand as Db.PlotDialog;               
        this.content.string = this.fullContent.shift() ;

        if(dialog.phiz){
            //如果表情存在
            var atlas:cc.SpriteAtlas = cc.loader.getRes("Phiz/"+dialog.name,cc.SpriteAtlas);
          
            if(atlas){
                this.phiz.spriteFrame = atlas.getSpriteFrame(dialog.phiz.toString());

                if(dialog.phizDir==1){                    
                    this.phizWidget.left = 0;                    
                    this.phizWidget.isAlignLeft = true;
                    this.phizWidget.isAlignRight = false;                                        
                    this.phizWidget.updateAlignment();
                }else if(dialog.phizDir==2){
                    this.phizWidget.right = 0;
                    this.phizWidget.isAlignLeft = false;
                    this.phizWidget.isAlignRight = true;                                        
                    this.phizWidget.updateAlignment();
                }
            }
        }else{
            this.phiz.spriteFrame = null;
        }       
        var offsexLW = 20;
        var offsexRW = 20;        
        if(this.phiz.spriteFrame){            
            var w = this.phiz.spriteFrame.getRect().width;
            if(dialog.phizDir==2){
                offsexRW = w
            }else{
                offsexLW = w;
            }            
        }        
        this.boxWidget.left = offsexLW;
        this.boxWidget.right = offsexRW;    
        this.boxWidget.updateAlignment();        
        this.node.opacity = 0;

        var a1 = cc.fadeIn(0.5);   
      
        this.node.runAction( a1 );
    }

    hide(){        
        var a1 = cc.fadeOut(0.5);
        var an = cc.callFunc(()=>{
            this.end()            
        }) 
        this.node.runAction(cc.sequence(a1,an));
    }
}
