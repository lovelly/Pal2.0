const {ccclass, property} = cc._decorator;

@ccclass
export default class PackItem extends cc.Component {
    /** 背包物品ID */    
    private _itemId:number;
    /** 物品数量 */
    private _itemCount:number;
    
    set itemId(value){
        this._itemId = value;
        this.item = Db.getItem(value);
        if(this.item){
            this.node.name = this.item.id.toString();
        }else{
            this.node.name = "packItem";
        }
        this.show();
    }
    get itemId(){
        return this._itemId;
    }
    set itemCount(value){
        this._itemCount = value;
        this.show();
    }
    get itemCount(){
        return this._itemCount;
    }


    /** 物品数据 */
    private _item:Db.Item;
    set item(value){
        this._item = value;
    }
    get item(){
        return this._item;
    }




    private icon:cc.Sprite;
    private count:cc.Label;
    onLoad(){
        this.icon = this.node.getChildByName("icon").getComponent(cc.Sprite);
        this.count = this.node.getChildByName("count").getComponent(cc.Label);
    }

    show(){
        if(!this.item){
            this.icon.spriteFrame = null;
            this.count.string = " ";      
            return;
        }

        if(this.itemCount<=0){
            this.icon.spriteFrame = null;
            this.count.string = " ";
            this.item = null;
            this.node.name = "packItem";
            return;
        }

        var atlas:cc.SpriteAtlas = cc.loader.getRes("Item/Items",cc.SpriteAtlas);
        var name = this.item.texture||this.item.name;
        this.icon.spriteFrame = atlas.getSpriteFrame(name);

        if(this.itemCount>1){
            this.count.string = this.itemCount.toString()
        }else{
            this.count.string = " ";
        }
    }
}
