import BaseDialog from "./Dialog/BaseDialog";
import AsideDialog from "./Dialog/AsideDialog";

const {ccclass, property} = cc._decorator;
@ccclass
export default class DialogPanel extends cc.Component {
    private sceneTip:BaseDialog;
    private dialog:BaseDialog;
    private asideDialog:AsideDialog;
    onLoad(){
        this.sceneTip = this.node.getChildByName("SceneTip").getComponent(BaseDialog);                
        this.dialog = this.node.getChildByName("Dialog").getComponent(BaseDialog);         
        this.asideDialog = this.node.getChildByName("Aside").getComponent(AsideDialog);
        
    }

    start(){
        this.sceneTip.node.active = false;
        this.dialog.node.active = false;     
        this.asideDialog.node.active = false;
    }

    show(plot:Db.Plot){        
        switch(plot.type){
            case "sceneTip":
                this.sceneTip.plot = plot;                
            break;            
            case "dialog":
                this.dialog.plot = plot;
            break;
            case "aside":
                this.asideDialog.plot = plot;
            break;
        }
        
    }

   
}
