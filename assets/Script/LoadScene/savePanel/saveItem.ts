const {ccclass, property} = cc._decorator;
@ccclass
export default class saveItem extends cc.Component {
    private no:cc.Label;
    private map:cc.Label;
    private gameTime:cc.Label;
    private type:cc.Node;
    private dateTime:cc.Label;

    private _saveItem;
    set saveItem(value){
        this._saveItem = value;        
        this.show()
    }
    get saveItem(){
        return this._saveItem;
    }

    onLoad(){
        var right = this.node.getChildByName("right");
        this.no = right.getChildByName("l1").getChildByName("no").getComponent(cc.Label);
        this.map = right.getChildByName("l1").getChildByName("map").getComponent(cc.Label);
        this.gameTime = right.getChildByName("l2").getChildByName("gametime").getComponent(cc.Label);
        this.type = right.getChildByName("l2").getChildByName("type");
        this.dateTime = right.getChildByName("l3").getChildByName("datetime").getComponent(cc.Label);


        var index = this.node.getSiblingIndex();
        this.no.string = "No."+(index+1);
        this.map.string = "";        
        this.gameTime.string = "";
        if(index>0){
            this.type.active = false;
        }
        this.dateTime.string = "";    


        this.node.on(cc.Node.EventType.TOUCH_END,this.touchEnd,this);
    }


    show(){
        if(!this.saveItem) return;             
        this.map.string = this.saveItem.map;
        this.gameTime.string = "游戏时间 "+ parseTimer(this.saveItem.gameTime) ;
        this.dateTime.string = new Date().Format("yyyy-MM-dd hh:mm:ss");
    }

    /** 点击保存 */
    touchEnd(){        
        // var game = QM.game.toObj();
        // var saveAll = cc.sys.localStorage.getItem("save");
        // if(saveAll){
        //     saveAll = JSON.parse(saveAll);
        // }else{
        //     saveAll = [];
        // }

        // var index = this.node.getSiblingIndex();
        // saveAll[index] = game
        // cc.sys.localStorage.setItem("save",JSON.stringify(saveAll));
        // this.saveItem = game;      
        
      
        // if(index+1>=this.node.parent.childrenCount){
        //     var event = new cc.Event.EventCustom("AddSaveItem",true);
        //     this.node.dispatchEvent(event);
        // }

        var event = new cc.Event.EventCustom("SaveOrLoad",true);
        this.node.dispatchEvent(event);
    }

}
