import BasePanel from "./BasePanel";
import DirController from "./DirController";

const {ccclass, property} = cc._decorator;

@ccclass
export default class MenuManager extends cc.Component {
    
    private menuPanel:cc.Node;

    public dirController:DirController;
    public menuNode:cc.Node;

    onLoad(){       
        if(!cc.game.isPersistRootNode(this.node)){
            cc.game.addPersistRootNode(this.node);
        }     
        this.menuPanel = this.node.getChildByName("MenuPanel");

        this.dirController = this.node.getChildByName("dirController").getComponent(DirController);
        this.menuNode = this.node.getChildByName("menu");

        this.node.setSiblingIndex(100);

        var manager = cc.director.getCollisionManager();
        manager.enabled = true;
        manager.enabledDebugDraw = true;
        //manager.enabledDrawBoundingBox=true;                                 
    
        //游戏正式开始,记录游戏时间
        this.schedule(()=>{
            QM.game.gameTime++;
        },1)


        //监听子菜单点击事件
        this.node.on("MenuClicked",this.OpenMenuPanel,this);
    }

    /**
     * 打开菜单面板
     */
    OpenMenuPanel(event:cc.Event.EventCustom){
        //暂停当前主逻辑
        cc.director.pause();        
        var name = event.target.name;
        cc.log(name);

        var panel = this.menuPanel.getChildByName(name+"Panel");
        if(panel){
            panel.getComponent(BasePanel).show();
        }        
    }

    start(){        
        this.hide();        
    }

    hide(){
        this.dirController.node.active = false;
        this.menuNode.active = false;
    }
}
