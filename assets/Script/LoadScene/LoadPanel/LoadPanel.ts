import BasePanel from "../BasePanel";
import saveItem from "../SavePanel/saveItem";

const {ccclass, property} = cc._decorator;

@ccclass
export default class LoadPanel extends BasePanel {
    @property(cc.Prefab)
    saveItemPrefab:cc.Prefab=null;
    private saveItemParent:cc.Node;
    private layout:cc.Layout;
    private widget:cc.Widget;
    onLoad(){
        this.close;
        this.saveItemParent = this.node.getChildByName("scrollview").getChildByName("view").getChildByName("content");
        this.layout = this.saveItemParent.getComponent(cc.Layout);
        this.widget = this.saveItemParent.getComponent(cc.Widget);

        /** 读取存档 */
        this.node.on("SaveOrLoad",this.Load,this);

    }

    firstCall(){    
       
        //初始化存档
        var saveAll = cc.sys.localStorage.getItem("save");                
        if(saveAll){
            saveAll = JSON.parse(saveAll);
           
            saveAll.forEach( (item,key)=>{
                var node = this.saveItemParent.children[key];
                if(!node){
                    node = cc.instantiate(this.saveItemPrefab);   
                    node.parent = this.saveItemParent;             
                }            
                var com = node.getComponent(saveItem);            
                com.saveItem = item;
            })
        }          
        this.layout.updateLayout();
        this.widget.updateAlignment();
    }    

    Load(event:cc.Event.EventCustom){
        var node:cc.Node = event.target;
        var save = node.getComponent(saveItem).saveItem;
        QM.game.loadObj(save);
        cc.director.resume();
        cc.director.loadScene("LoadScene");        
        this.node.active = false;
    }
}
