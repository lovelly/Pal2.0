const {ccclass, property} = cc._decorator;

@ccclass
export default class MenuItem extends cc.Component {
    private child:cc.Node
    onLoad(){
        this.child = this.node.getChildByName("child");        
        this.node.on(cc.Node.EventType.TOUCH_END,this.clickMenu,this);
    }

    /**
     * 菜单点击
     */
    clickMenu(){        
        if(this.child){
            this.child.active = !this.child.active;
        }else{
            //传递菜单点击事件
            var event = new cc.Event.EventCustom("MenuClicked",true);
            this.node.dispatchEvent(event);
        }
    }
}
