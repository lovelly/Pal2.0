const {ccclass, property} = cc._decorator;

@ccclass
export default class LoadScene extends cc.Component {

    /** 总共加载资源组 */
    private resGroup=[
        "Prefab",
        "Avatar",
        "Item",
        "Phiz"
    ]
    /** 已加载多少个组 */
    private resGroupIndex=0;

    onLoad(){        
        this.resGroupIndex=0;
        //加载通用资源
        this.resGroup.forEach(path=>{
            cc.loader.loadResDir(path,(err,res)=>{ this.next();})
        })
    }


    next(){
        this.resGroupIndex+=1;
        if(this.resGroupIndex<this.resGroup.length) return;
        var mapName = QM.game.map;        
        cc.director.loadScene(mapName);    
    }
}
