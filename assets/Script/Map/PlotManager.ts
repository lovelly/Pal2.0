import MapManager from "./MapManager";
import Character from "./Character";

const {ccclass, property} = cc._decorator;

@ccclass
export default class PlotManager extends cc.Component {
    private mapManager:MapManager
    private plots:Db.Plot[];
    
    onLoad(){
        this.mapManager = this.getComponent(MapManager);    
        
        this.node.parent.on("DialogEnd",this.ItemEnd,this);
        /** 触发门的剧本 */
        this.node.parent.on("DoorPlot",this.DoorPlot,this);
        /** 触发NPC的剧本 对话 */
        this.node.parent.on("NpcPlot",this.NpcPlot,this);
        /** 获取物品提示 */
        this.node.parent.on("GetItemPlot",this.GetItemPlot,this);
    }

    start(){        
    }


    /** 门禁 */
    DoorPlot(event:cc.Event.EventCustom){
        var plots = event.getUserData() as Db.Plot[];
        this.parse(plots);
    }
    /** Npc对话剧本 */
    NpcPlot(event:cc.Event.EventCustom){
        var plots = event.getUserData() as Db.Plot[];        
        this.parse(plots);
    }
    GetItemPlot(event:cc.Event.EventCustom){
        var plots = event.getUserData() as Db.Plot[];        
        this.parse(plots);
    }

    parse(plots:Db.Plot[]){
        this.plots = plots;
        this.hideUI();
        this.startItem()
    }


    startItem(){
        if(!this.plots.length){
            this.showUI();
            return;
        } 
        var plot = this.plots.shift();         
        switch(plot.class){
            case "move":
                this.move(plot);
            break;
            case "dialog":
                this.dialog(plot);
            break;
            case "director":
                //转场
                this.director(plot);
            break;
            case "dat":
                //解析数据
                this.parseDat(plot);
            break;
        }
    }

    /** 移动对象 */
    move(plot:Db.Plot){
        let expand:Db.PlotMove = plot.expand as Db.PlotMove;        
        //找查对象    
        var target = this.mapManager.node.getChildByName(expand.name);                
        var ch = target.getComponent(Character);
        var ps = this.mapManager.findPath(expand.path);
        
        var pStart = target.position;        
        ch.action = 2;
        var tempActionRow = [];        
        ps.forEach(pTarget=>{
                var angle = cc.v2(1,0).signAngle( pTarget.sub(pStart) )*180/Math.PI;            
                var dir =  QM.getDirForAngle(angle);            
                var a1 = cc.callFunc(()=>{            
                    ch.dir = dir;
                })                
                var a2 = cc.moveTo(pTarget.sub(pStart).mag()/100,pTarget);
                tempActionRow.push(a1,a2);    
                pStart = pTarget;
        })
        var a3 = cc.callFunc(()=>{
            ch.action = 1;
        })
        tempActionRow.push(a3); 
      
        if(plot.stopUpEvent){
            this.startItem();
        }else{
            var an = cc.callFunc(()=>{
                this.startItem();
            })
            tempActionRow.push(an);    
        }
        

        var seq = cc.sequence(tempActionRow);
        target.runAction(seq);
    }
    /** 对话 */
    dialog(plot:Db.Plot){
        this.mapManager.dialogPanel.show(plot);
    }
    /** 转场 */
    director(plot:Db.Plot){        
        var expand = plot.expand as Db.PlotDirector;
        //转场
        QM.game.map = expand.name;
        if(expand.door){
            QM.game.door = expand.door;
        }
        cc.director.loadScene("LoadScene");
    }

    /**
     * 解析数据
     * type 
     *      addTask 接受任务
     *      npcPlot 修改npc剧本    
     */
    parseDat(plot:Db.Plot){
        switch(plot.type){
            case "task":            
                var taskId = (plot.expand as Db.PlotDatTask).taskId;
                QM.game.addTask(taskId);
                this.ItemEnd();
            break;
            case "npc":
                //修改地图中Npc的剧本数据                
                let expandNpc = plot.expand as Db.PlotDatNpc;
                if(expandNpc.cmd=="delete"){
                    this.mapManager.removeNpc(expandNpc.name);                 
                }else{
                    QM.game.updateNpc(expandNpc);
                }                
                this.ItemEnd();
            break;
            case "door":              
                QM.game.updateDoor(plot.expand as Db.PlotDatDoor);
                this.ItemEnd();
            break;
            case "pack":
                //先修改背包中的数据
                let expand = plot.expand as Db.PlotDatPack;
                if(expand.point){
                    //将物品放置到地图上
                    this.mapManager.addMapItem({
                        "itemId":expand.itemId,
                        "point":expand.point,
                        "isPickUp":expand.isPickUp||false,
                        "show":true
                    });
                }
                QM.game.updatePackItem(expand);
                this.ItemEnd();
            break;
        }
    }

    hideUI(){
        this.mapManager.hideUI();
    }

    showUI(){
        this.mapManager.showUI();
    }

    ItemEnd(){
        this.startItem();
    }

    
}
