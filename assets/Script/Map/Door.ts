const {ccclass, property} = cc._decorator;

@ccclass
export default class Door extends cc.Component {
    @property({
        displayName:"是否开门"
    })
    isOpen:boolean=true;

    //如果大门关闭，最好设置门禁
    plot:Db.Plot[];
    @property({
        displayName:"大门方向",
        tooltip:"1:上\n2:下\n3:左\n4:右\n5:左上\n6:右下\n7:左下\n8:右上",
        range:[1,8,1]
    })
    dir:QM.Dir=1

    @property({
        displayName:"进入地图"
    })
    map:string=""

    /** 门前点 */
    private _point:cc.Node;
    get point(){
        if(!this._point){
            this._point = this.node.getChildByName("point");
        }
        return this._point;
    }
    /** 获取大门前的位置 */
    get gatePisition():cc.Vec2{
        return this.node.convertToWorldSpaceAR(this.point.position);
    }

    /** 门的基础数据 */
    private _door:QM.Door
    set door(value){
        this._door = value
    }
    get door(){
        return this._door
    }

    /** 重置门 */
    reset(door:QM.Door){
        this.door = door;      
    }

    onCollisionEnter(other,self){                        
        var isOpen = this.door?this.door.isOpen:this.isOpen;
        var map = this.door?this.door.map:this.map;

        //当碰撞到门后.....
        //1判断门是否已开        
        if(isOpen){
            //进入新的地图  
            QM.game.map = map;
            QM.game.door = this.node.name;          
            cc.director.loadScene("LoadScene");
        }else{
            var plot = this.door?this.door.plot:this.plot;
            //显示门禁     TODO            
            var event = new cc.Event.EventCustom("DoorPlot",true);
            event.setUserData([...plot]);
            this.node.dispatchEvent(event);
        }
    }
}
