const {ccclass, property} = cc._decorator;

@ccclass
export default class Character extends cc.Component {
    /** 角色的当前动作 
     * 1 待机
     * 2 移动
    */
   private _action:number;
   set action(value){
       if(this.action==value) return;
       this._action = value;
       this.playAnim();
   }
   get action(){
       return this._action;
   }

   protected lastDir:QM.Dir;
   /** 角色当前的方向 */   
   private _dir:QM.Dir;
   set dir(value){
        
        if(value==0){
           this.action = 1;
           return;
        }
       if(this.dir==value) return;
        this.lastDir = this.dir;      
       this._dir = value   
       this.playAnim()
   }
   get dir(){
       return this._dir;
   }  
  
  
    /**
     * 移动
     */
    Automove(){
        cc.log("npc移动");
    }

    zIndex(){
        this.node.zIndex = this.node.parent.height-this.node.position.y;
    }

    playAnim(){}
}
