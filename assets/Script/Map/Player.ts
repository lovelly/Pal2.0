import Character from "./Character";

const {ccclass, property} = cc._decorator;

@ccclass
export default class Player extends Character {
    /** player源数据 */
    private _player:QM.Player;
    set player(value){
        this._player = value;
        this.action = value.action;
        this.dir = value.dir;
        if(value.position){                        
            this.node.position = cc.v2(value.position);
        }
        if(value.number==1){
            this.boxCollider.enabled = true
        }else{
            this.boxCollider.enabled = false;
        }
        this.node.name = value.name;        
        this.init();
    }
    get player(){
        return this._player;
    }

    /** 随从 */
    follower:Player;

    /** 上级的位置 */
    brotherPosition:cc.Vec2;
    /** 上级的方向 */
    brotherDir:QM.Dir;

    /** 动画组件 */
    private animCom:cc.Animation
    private boxCollider:cc.BoxCollider;

    /** 是否碰撞了 */
    private isCollision:boolean=false;
    private lastPosition:cc.Vec2;
    private collisionBox:cc.Vec2[];


    onLoad(){
        this.animCom = this.getComponent(cc.Animation);
        this.boxCollider = this.getComponent(cc.BoxCollider);
    }



    init(){       
        //加载player的图片资源
        cc.loader.loadRes("Player/"+this.player.name+"/"+this.player.texture,cc.SpriteAtlas,(err,atlas:cc.SpriteAtlas)=>{
            var spFrames = atlas.getSpriteFrames();
            spFrames.sort((a,b)=>{
                return Number(a.name)-Number(b.name) 
            })
            
            //01-08为八方向图            
            for(var i=1;i<=8;i++){
                var sp = spFrames.splice(0,1);    
                var clip = cc.AnimationClip.createWithSpriteFrames(sp as [cc.SpriteFrame],1)
                clip.name = "1_"+i;
                this.animCom.addClip(clip)
            }
            
            //行走图，看剩下总数
            var n = spFrames.length/8;
            for(var i=1;i<=8;i++){
                var sp = spFrames.splice(0,n);
                var clip = cc.AnimationClip.createWithSpriteFrames(sp as [cc.SpriteFrame],n)
                clip.wrapMode = cc.WrapMode.Loop;
                clip.speed = 2;
                clip.name = "2_"+i;
                this.animCom.addClip(clip);
            }                                    

            this.playAnim();
        })

        this.zIndex();    
    }


    playAnim(){
        var action = this.action;
        var dir = this.dir;
        if(!action || !dir) return;
        var name = action+"_"+dir;
        //获取当前正在播放的动画名称        

        var currentClip = this.animCom.currentClip;
        if(currentClip && currentClip.name==name) return;        
        
        this.animCom.play(name);
    }

    move(dir:QM.Dir){  
        if(!this.node) return;
        if(dir==0){
            this.action = 1;
            if(this.follower){
                this.follower.followerMove(0,this.node.position);
            }
            return;
        }
        this.action = 2;
        this.dir = dir;
     
    }

    /** 随从的移动 */
    followerMove(dir:QM.Dir,p:cc.Vec2){
        this.dir = dir;        
        if(dir>0){
            this.action = 2;
        }else{
            this.action = 1;
            return;
        }
        //0.07测试出来的数值，可能是跟队长每帧移动的距离有关系
        this.node.position = this.node.position.lerp(p,0.07) ;
        this.zIndex();
    }

    update(){
        this.updateGamePlayer();        
        if(this.action==2){
            if(!this.isCollision){
                var dir = this.dir;
                var angle;
                switch(dir){
                    case QM.Dir.rightUp:
                        angle=45;
                    break
                    case QM.Dir.up:
                        angle=90;
                    break
                    case QM.Dir.leftUp:
                        angle=135
                    break
                    case QM.Dir.left:
                        angle = 180
                    break
                    case QM.Dir.leftDown:
                        angle = -135
                    break
                    case QM.Dir.down:
                        angle = -90
                    break
                    case QM.Dir.rightDown:
                        angle = -45
                    break
                    case QM.Dir.right:
                        angle = 0
                    break
                }
                var p1 = this.node.position;           
                var p2 = cc.Vec2.RIGHT.rotate(angle*Math.PI/180).mul(2);
                var p3 = p1.add(p2);
                
                //cc.log(this.lastPosition.sub(p3).mag());
              
                var isCanMove = true;
                if(this.collisionBox){
                    if(cc.Intersection.pointInPolygon(p3,this.collisionBox)){                         
                        isCanMove = false;
                    }
                }
                
                if(isCanMove){
                    this.node.position = p3;                 
                }
                
                
                
                this.zIndex();
                
        
                //每次移动时都要通知随从 通知内容包括当前的位置与方向
                if(this.follower){
                    this.follower.followerMove(this.dir,this.node.position);
                } 
            }
        }  
               
      
    }

    lateUpdate(){     
        


        if(this.isCollision){
            this.node.position = this.lastPosition ;
            return;
        }
        this.lastPosition = this.node.getPosition();  
        //cc.log("lastPositon",this.lastPosition);
    }


    /** 更新游戏数据 */
    updateGamePlayer(){
        this.player.action = this.action;
        this.player.dir = this.dir;
        this.player.position = this.node.position
    }

    onCollisionEnter(other,self){  
      
        this.collisionBox = other.world.points;
        this.isCollision = true;
    }
    onCollisionExit(){     
        this.isCollision = false;
    }
    
}
