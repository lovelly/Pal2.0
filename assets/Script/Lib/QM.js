"use strict";
var QM;
(function (QM) {
    /** 方向 */
    var Dir;
    (function (Dir) {
        /** 上 */
        Dir[Dir["up"] = 1] = "up";
        /** 下 */
        Dir[Dir["down"] = 2] = "down";
        /** 左 */
        Dir[Dir["left"] = 3] = "left";
        /** 右 */
        Dir[Dir["right"] = 4] = "right";
        /** 左上 */
        Dir[Dir["leftUp"] = 5] = "leftUp";
        /** 右下 */
        Dir[Dir["rightDown"] = 6] = "rightDown";
        /** 左下 */
        Dir[Dir["leftDown"] = 7] = "leftDown";
        /** 右上 */
        Dir[Dir["rightUp"] = 8] = "rightUp";
    })(Dir = QM.Dir || (QM.Dir = {}));
    function getDirForAngle(angle) {
        var dir;
        if (angle >= -22.5 && angle < 22.5) {
            dir = QM.Dir.right;
        }
        else if (angle >= 22.5 && angle < 67.5) {
            dir = QM.Dir.rightUp;
        }
        else if (angle >= 67.5 && angle < 112.5) {
            dir = QM.Dir.up;
        }
        else if (angle >= 112.5 && angle < 157.5) {
            dir = QM.Dir.leftUp;
        }
        else if (angle >= 157.5 || angle < -157.5) {
            dir = QM.Dir.left;
        }
        else if (angle >= -157.5 && angle < -112.5) {
            dir = QM.Dir.leftDown;
        }
        else if (angle >= -112.5 && angle < -67.5) {
            dir = QM.Dir.down;
        }
        else if (angle >= -67.5 && angle < -22.5) {
            dir = QM.Dir.rightDown;
        }
        return dir;
    }
    QM.getDirForAngle = getDirForAngle;
    /** 游戏状态 */
    var gameState;
    (function (gameState) {
        /** 正常 */
        gameState[gameState["normal"] = 1] = "normal";
        /** 剧本控制中  */
        gameState[gameState["Plot"] = 2] = "Plot";
    })(gameState = QM.gameState || (QM.gameState = {}));
    var Game = /** @class */ (function () {
        function Game() {
            this._events = {};
            /** 游戏状态
             * normal:正常
             * UI:UI显示中
            */
            this.state = gameState.normal;
            /** 当前地图 */
            this.map = "杭州城";
            this.gameTime = 0;
            /** 地图信息 */
            this.mapDat = {};
            /** 队伍信息 */
            this.team = [
                {
                    name: "王小虎",
                    texture: "base",
                    action: 1,
                    dir: QM.Dir.down,
                    number: 1,
                    position: { x: 1145, y: 158 },
                    attr: {
                        level: 1,
                        exp: 0,
                        hp: {
                            current: 150,
                            max: 150
                        },
                        mp: {
                            current: 100,
                            max: 100
                        },
                        physical: 31,
                        magic: 25,
                        defense: 23,
                        bodylaw: 30,
                        luck: 20
                    },
                    equipment: {
                        hat: 200,
                        cloak: 400,
                        clothes: 500,
                        arms: 1,
                        wear: 600,
                        shoes: 800
                    }
                }
            ];
            /** 背包物品 */
            this.pack = {};
            /** 任务 */
            this.task = {};
            if (Game.Instance)
                return;
        }
        Game.prototype.on = function (event, callback, obj) {
            this._events[event] = { func: callback, obj: obj };
        };
        Game.prototype.off = function (event) {
            if (this._events[event]) {
                delete this._events[event];
            }
        };
        /** 获取地图信息 */
        Game.prototype.getMapDat = function (name) {
            var temp = name || this.map;
            return this.mapDat.hasOwnProperty(temp) ? this.mapDat[temp] : null;
        };
        /** 设置地图信息 */
        Game.prototype.setMapDat = function (dat, name) {
            var temp = name || this.map;
            this.mapDat[temp] = dat;
        };
        /** 获取团队所有队员 */
        Game.prototype.getTeam = function () {
            return this.team;
        };
        /** 获取背包内所有物品 */
        Game.prototype.getPack = function () {
            return this.pack;
        };
        /** 背包内是否有某物品 返回物品数量 */
        Game.prototype.isPackItem = function (itemId) {
            return this.pack.hasOwnProperty(itemId) ? true : false;
        };
        /** 给背包中添加物品 count个 */
        Game.prototype.addPackItem = function (itemId, count) {
            if (count === void 0) { count = 1; }
            var c = this.pack[itemId] || 0;
            var sum = count + c;
            this.pack[itemId] = sum;
            if (this._events["packItem"]) {
                var qe = {
                    name: "add",
                    data: {
                        id: itemId,
                        count: sum
                    }
                };
                var func = this._events.packItem.func;
                var obj = this._events.packItem.obj;
                func.call(obj, qe);
            }
        };
        /** 移除背包中某物品N个 */
        Game.prototype.subPackItem = function (itemId, count) {
            if (count === void 0) { count = 1; }
            var current = this.pack[itemId] - count;
            if (current <= 0) {
                delete this.pack[itemId];
            }
            else {
                this.pack[itemId] = current;
            }
            if (this._events["packItem"]) {
                var qe = {
                    name: "sub",
                    data: {
                        id: itemId,
                        count: current
                    }
                };
                var func = this._events.packItem.func;
                var obj = this._events.packItem.obj;
                func.call(obj, qe);
            }
        };
        /** 清空背包中的某物品 */
        Game.prototype.clearPackItem = function (itemId) {
            var qe = {
                name: "sub",
                data: {
                    id: itemId,
                    count: 0
                }
            };
            delete this.pack[itemId];
            var func = this._events.packItem.func;
            var obj = this._events.packItem.obj;
            func.call(obj, qe);
        };
        Game.prototype.updatePackItem = function (packItemDat) {
            var cmd = packItemDat.cmd;
            var itemId = packItemDat.itemId;
            var number = packItemDat.number || 1;
            switch (cmd) {
                case "add": //添加物品
                    this.addPackItem(itemId, number);
                    break;
                case "sub": //减少一个物品
                    this.subPackItem(itemId, number);
                    break;
                case "remove": //移除所有的物品
                    this.clearPackItem(itemId);
                    break;
            }
        };
        /** 接受一个任务 */
        Game.prototype.addTask = function (id) {
            if (this.task[id])
                return;
            this.task[id] = 1;
            if (this._events["task"]) {
                var qe = {
                    name: "AddTask",
                    data: {
                        id: id
                    }
                };
                var func = this._events.task.func;
                var obj = this._events.task.obj;
                func.call(obj, qe);
            }
        };
        /** 获取所有的任务数据 */
        Game.prototype.getTaskAll = function () {
            return this.task;
        };
        /** 是否已接受某任务 */
        Game.prototype.isTask = function (taskId) {
            return this.task.hasOwnProperty(taskId) ? true : false;
        };
        /** 更新一个npc的数据 */
        Game.prototype.updateNpc = function (datNpc) {
            //先在地图数据中查找npc
            var mapDat = this.getMapDat(datNpc.map);
            var npcs = mapDat.npc;
            if (datNpc.cmd == "add") {
                //新增一个npc
                return;
            }
            var currentNpc;
            for (var i = 0; i < npcs.length; i++) {
                var npc = npcs[i];
                if (npc.name == datNpc.name) {
                    currentNpc = npc;
                    break;
                }
            }
            if (!currentNpc)
                return;
            if (datNpc.cmd == "update") {
                //修改一个npc的数据
                if (datNpc.plot) {
                    currentNpc.plot = datNpc.plot;
                }
                if (datNpc.needIf) {
                    currentNpc.needIf = datNpc.needIf;
                }
            }
            else if (datNpc.cmd == "delete") {
                //删除一个npc
            }
        };
        /** 更新门的数据 */
        Game.prototype.updateDoor = function (datDoor) {
            var mapDat = this.getMapDat(datDoor.map);
            var doors = mapDat.door;
            if (datDoor.cmd == "add") {
                //新增一个门
                return;
            }
            var currentDoor;
            for (var i = 0; i < doors.length; i++) {
                var door = doors[i];
                if (door.name == datDoor.name) {
                    currentDoor = door;
                    break;
                }
            }
            if (!currentDoor)
                return;
            if (datDoor.cmd == "update") {
                if (datDoor.door.hasOwnProperty("plot")) {
                    currentDoor.plot = datDoor.door.plot;
                }
                if (datDoor.door.hasOwnProperty("isOpen")) {
                    currentDoor.isOpen = datDoor.door.isOpen;
                }
            }
        };
        /** 提取出保存的数据 */
        Game.prototype.toObj = function () {
            var obj = {
                map: this.map,
                gameTime: this.gameTime,
                mapDat: this.mapDat,
                team: this.team,
                pack: this.pack,
                task: this.task
            };
            return obj;
        };
        /** 数据还原到game */
        Game.prototype.loadObj = function (obj) {
            this.map = obj.map;
            this.gameTime = obj.gameTime;
            this.mapDat = obj.mapDat;
            this.team = obj.team;
            this.pack = obj.pack;
            this.task = obj.task;
            this.door = null;
            this._events = {};
        };
        Game.Instance = new Game;
        return Game;
    }());
    QM.Game = Game;
    QM.game = new Game();
})(QM || (QM = {}));
