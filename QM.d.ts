declare namespace QM {
    /** 地图数据 */
    interface mapDat {
        npc?: Npc[];
        plot?: Db.Plot[];
        door?: Door[];
        item?: Item[];
    }
    /** Npc数据 */
    interface Npc {
        /** 名字 */
        name: string;
        /** 显示图 */
        texture: string;
        /** 出生位置 */
        point?: string;
        /** 移动路径(巡场走动) */
        path?: string;
        /** 动作 */
        action: number;
        /** 方向 */
        dir: QM.Dir;
        /** 剧本 */
        plot?: Db.Plot[];
        /** 满足条件时,修改剧情
         * if  taskId 接受了ID任务 itemId 拥有某ID物品
         */
        needIf?: NeedIf;
    }


    /** 怪物军团 */
    interface EnemyCorps{
        /** 图形 */
        texture:string
        /** 巡逻半径 */
        radius:number
        /** 警戒半径 */
        warn:number
        /** 移动速度 */
        speed:number
        /** 追击速度 */
        run:number
        /** 出生点 */
        point:string
        /** 固定出现怪物 与range互斥 */
        enemy?:number[]
        /** 随机出现怪物 与enemy互斥 */
        range?:number[]
    }

    /** 
     * 怪物素材分析 
     * 1 待机图片 张数不定  101-10?待机
     * 2 攻击图片          201-20?攻击
     * 3 魔法图片          301-30?魔法1
     * 4 魔法图片          401-40?魔法2
     *      
     * t 攻击特效 可无     t201-t20?攻击物资
     * t 魔法特效 可无     t301-t30?魔法特效
     */



    /** 满足某些条件，修改剧情 */
    interface NeedIf {
        if: {
            taskId?: number;
            itemId?: number;
        };
        plot: Db.Plot[];
    }
    /** Door门数据结构 */
    interface Door {
        name: string;
        /** 进入的地图 */
        map?: string;
        /** 是否开门 */
        isOpen?: boolean;
        /** 门禁 */
        plot?: Db.Plot[];
    }
    /** 地图 物品数据 */
    interface Item {
        itemId: number;
        point: string;
        /** 是否可以拾取 */
        isPickUp?: boolean;
        show?: boolean;
    }
    /** 方向 */
    enum Dir {
        /** 上 */
        up = 1,
        /** 下 */
        down = 2,
        /** 左 */
        left = 3,
        /** 右 */
        right = 4,
        /** 左上 */
        leftUp = 5,
        /** 右下 */
        rightDown = 6,
        /** 左下 */
        leftDown = 7,
        /** 右上 */
        rightUp = 8,
    }
    function getDirForAngle(angle: number): number;
    /**
     * 队友
     */
    interface Player {
        /** 名称 */
        name: string;
        /** 图源 */
        texture: string;
        /** 动作 */
        action: number;
        /** 方向 */
        dir: QM.Dir;
        /** 编号 1号为队长 */
        number: number;
        /** 坐标 */
        position: {
            x: number;
            y: number;
        };
        attr: {
            /** 等级 */
            level: number;
            /** 当前经验 */
            exp: number;
            /** 体力 */
            hp: {
                /** 当前体力 */
                current: number;
                /** 最大体力 */
                max: number;
            };
            /** 真气 */
            mp: {
                /** 当前真气 */
                current: number;
                /** 最大真气 */
                max: number;
            };
            /** 物理攻击力 武术 */
            physical: number;
            /** 魔法攻击力 灵力 */
            magic: number;
            /** 防御 */
            defense: number;
            /** 身法 */
            bodylaw: number;
            /** 吉运 */
            luck: number;
        };
        /** 装备 */
        equipment: {
            /** 帽子 */
            hat: number;
            /** 披风护肩 */
            cloak: number;
            /** 衣服 */
            clothes: number;
            /** 武器 */
            arms: number;
            /** 佩带 */
            wear: number;
            /** 鞋子 */
            shoes: number;
        };
    }
    /** 背包物品 */
    interface PackItem {
        [id: number]: number;
    }
    /** 事件 */
    interface QEvent {
        name: string;
        data: any;
    }
    /** 游戏状态 */
    enum gameState {
        /** 正常 */
        normal = 1,
        /** 剧本控制中  */
        Plot = 2,
    }
    class Game {
        private static readonly Instance;
        private _events;
        /** 游戏状态
         * normal:正常
         * UI:UI显示中
        */
        state: gameState;
        /** 当前地图 */
        map: string;
        gameTime: number;
        /** 上次进入过的门 */
        door: string;
        /** 地图信息 */
        private mapDat;
        /** 队伍信息 */
        private team;
        /** 背包物品 */
        private pack;
        /** 任务 */
        private task;
        constructor();
        on(event: string, callback: any, obj: any): void;
        off(event: string): void;
        /** 获取地图信息 */
        getMapDat(name?: string): mapDat | null;
        /** 设置地图信息 */
        setMapDat(dat: any, name?: string): void;
        /** 获取团队所有队员 */
        getTeam(): Player[];
        /** 获取背包内所有物品 */
        getPack(): PackItem;
        /** 背包内是否有某物品 返回物品数量 */
        isPackItem(itemId: number): boolean;
        /** 给背包中添加物品 count个 */
        private addPackItem(itemId, count?);
        /** 移除背包中某物品N个 */
        private subPackItem(itemId, count?);
        /** 清空背包中的某物品 */
        private clearPackItem(itemId);
        updatePackItem(packItemDat: Db.PlotDatPack): void;
        /** 接受一个任务 */
        addTask(id: any): void;
        /** 获取所有的任务数据 */
        getTaskAll(): {
            [id: number]: number;
        };
        /** 是否已接受某任务 */
        isTask(taskId: any): boolean;
        /** 更新一个npc的数据 */
        updateNpc(datNpc: Db.PlotDatNpc): void;
        /** 更新门的数据 */
        updateDoor(datDoor: Db.PlotDatDoor): void;
        /** 提取出保存的数据 */
        toObj(): {
            map: string;
            gameTime: number;
            mapDat: {
                [key: string]: mapDat;
            };
            team: Player[];
            pack: PackItem;
            task: {
                [id: number]: number;
            };
        };
        /** 数据还原到game */
        loadObj(obj: any): void;
    }
    let game: Game;
}
